<script>
    $(document).ready(function () {
        $('body .main > main').addClass('page-settings');
    });
</script>

<?php
$show_inner = false;

if (isset($_GET['group']) and $_GET['group']) {
    $group = $_GET['group'];

    if ($group == 'general') {
        $show_inner = 'settings/group/website';
    } elseif ($group == 'updates') {
        $show_inner = 'updates';
    } elseif ($group == 'email') {
        $show_inner = 'settings/group/email';
    } elseif ($group == 'template') {
        $show_inner = 'settings/group/template';
    } elseif ($group == 'advanced') {
        $show_inner = 'settings/group/advanced';
    } elseif ($group == 'files') {
        $show_inner = 'files/admin';
    } elseif ($group == 'login') {
        $show_inner = 'settings/group/users';
    } elseif ($group == 'language') {
        $show_inner = 'settings/group/language';
    }
    //elseif ($group == 'privacy') {
      //  $show_inner = 'settings/group/privacy';
    //}
    elseif ($group == 'searchhits') {
        $show_inner = 'settings/group/searchhits';
    }elseif ($group == 'postlimit') {
        $show_inner = 'settings/group/postlimit';
    }elseif ($group == 'blog_menu') {
        $show_inner = 'settings/group/blog_menu';
    }elseif ($group == 'seo') {
        $show_inner = 'settings/group/seo_settings';
    } else{
        $show_inner = false;
    }
}
?>

<?php if ($show_inner): ?>
    <module type="<?php print $show_inner ?>"/>
    <?php return; ?>
<?php endif ?>

<div class="card bg-none style-1 mb-0">
    <div class="card-header px-0">
        <h5><i class="mdi mdi-earth text-primary mr-3"></i> <strong><?php _e('Website settings'); ?></strong></h5>
        <div>

        </div>
    </div>

    <div class="card-body pt-3 px-0">
        <div class="card style-1 mb-3">
            <div class="card-body pt-3 px-5">
                <div class="row select-settings">
                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=general" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/1.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('General'); ?></span><br/>
                                <small class="text-muted"><?php _e('Make basic settings for your website'); ?></small>
                            </div>
                        </a>
                    </div>

                    <!-- <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=updates" class="d-flex my-3">
                            <div class="icon-holder"><i class="mdi mdi-flash-outline mdi-20px"></i></div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold">Updates</span><br/>
                                <small class="text-muted">Check for the latest updates</small>
                            </div>
                        </a>
                    </div> -->

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=email" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/8.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('E-mail'); ?></span><br/>
                                <small class="text-muted"><?php _e('Email settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=template" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/14.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Template'); ?></span><br/>
                                <small class="text-muted"><?php _e('Change or manage the theme you use'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=advanced" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/2.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Advanced'); ?></span><br/>
                                <small class="text-muted"><?php _e('Additional settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=files" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/9.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Files'); ?></span><br/>
                                <small class="text-muted"><?php _e('File management'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=login" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/15.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Login & Register'); ?></span><br/>
                                <small class="text-muted"><?php _e('Manage the access control to your website'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=language" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/3.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Language'); ?></span><br/>
                                <small class="text-muted"><?php _e('Choice of language and translations'); ?></small>
                            </div>
                        </a>
                    </div>

                    <!-- This module also hide and usersnap id is :167 -->
                    <!--
                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=privacy" class="d-flex my-3">
                            <div class="icon-holder"><i class="mdi mdi-shield-edit-outline mdi-20px"></i></div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold">Privacy Policy</span><br/>
                                <small class="text-muted">Privacy Policy and GDPR settings</small>
                            </div>
                        </a>
                    </div>
                    -->


                    <div class="col-12 col-sm-6 col-lg-4">
                        <a onclick="connectDRM()" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/01.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Connect to DRM'); ?></span><br/>
                                <small class="text-muted"><?php _e('Connect To DRM'); ?></small>
                            </div>
                        </a>
                    </div>


                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?load_module:users" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/4.png" style="height:20px;width:20px;" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Users'); ?></span><br/>
                                <small class="text-muted"><?php _e('Users settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="https://drm.software/admin/contact_forms" class="d-flex my-3" target="_blank">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/contact.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Contact-form'); ?></span><br/>
                                <small class="text-muted"><?php _e('Contact-form settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?load_module:comments" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/16.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Comments'); ?></span><br/>
                                <small class="text-muted"><?php _e('Comments settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?load_module:menu" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/5.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Menu'); ?></span><br/>
                                <small class="text-muted"><?php _e('Menu settings'); ?></small>
                            </div>
                        </a>
                    </div>


                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="https://drm.software/admin/email_marketings" class="d-flex my-3" target="_blank">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/02.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Dropfunnels'); ?></span><br/>
                                <small class="text-muted"><?php _e('Dropfunnels settings'); ?></small>
                            </div>
                        </a>
                    </div>

                     <!-- This module is hide and the usersnap id is:169 -->
                     <!--
                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?load_module:cookie_notice" class="d-flex my-3">
                            <div class="icon-holder"><img src="<?php print modules_url(); ?>cookie_notice.svg" style="height:20px;width:20px;" alt=""></div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold">Cookie Notice</span><br/>
                                <small class="text-muted">Cookie Notice settings</small>
                            </div>
                        </a>
                    </div>
                    -->

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?load_module:admin__backup_v2" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/6.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Backup'); ?></span><br/>
                                <small class="text-muted"><?php _e('Backup V2 settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?load_module:captcha" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/12.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Captcha'); ?></span><br/>
                                <small class="text-muted"><?php _e('Captcha settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?load_module:testimonials" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/17.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Testimonials'); ?></span><br/>
                                <small class="text-muted"><?php _e('Testimonials settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=searchhits" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/7.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Search Hits'); ?></span><br/>
                                <small class="text-muted"><?php _e('Search Hits settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=postlimit" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/13.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold"><?php _e('Post Limit'); ?></span><br/>
                                <small class="text-muted"><?php _e('Post Limit settings'); ?></small>
                            </div>
                        </a>
                    </div>

                    <div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=blog_menu" class="d-flex my-3">
                            <div class="icon-holder"><i class="mdi mdi-search-web mdi-20px"></i></div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold">Category Menu</span><br/>
                                <small class="text-muted">Category Menu settings</small>
                            </div>
                        </a>
                    </div>

					<div class="col-12 col-sm-6 col-lg-4">
                        <a href="?group=seo" class="d-flex my-3">
                            <div class="icon-holder">
                                <img src="<?php print modules_url(); ?>admin-logo/website-settings/seo.png" alt="">
                            </div>
                            <div class="info-holder">
                                <span class="text-primary font-weight-bold">SEO Settings</span><br/>
                                <small class="text-muted">Search Engine Optimization Settings.</small>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function connectDRM(){
        $('#myModal').show();
    }
</script>
