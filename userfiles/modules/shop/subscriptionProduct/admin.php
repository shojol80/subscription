 <!-- Page Header -->
    <div class="page-header">
        <div class="row align-items-center">
            <div class="col">
                <h3 class="page-title">Subscription Product</h3><br>
            </div>
           
        </div>
    </div>


    <!-- /Page Header -->
    <!-- add subscription interval -->
    <form method="post">
        <div class="form-row">
            <div class="form-group col-md-6">
                <input type="text" name="interval" id="interval" class="form-control" placeholder="Subscription Time Interval" required>
            </div>
            <div class="form-group col-md-4">
                <select id="freequency" class="form-control">
                    <option selected>Day</option>
                    <option>Week</option>
                    <option>Month</option>
                    <option>Year</option>
                </select>
            </div>
            <div class="form-group col-md-2">
                <a href="" onclick="add_new_item()" class="btn btn-primary add"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </form>
    

    <?php
        use Illuminate\Support\Facades\DB;
        $data=DB::table('subscription_items')->get()->all();


    ?>
 <div class="row">
        <div class="col-sm-12">
        
            <div class="card card-table">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0 datatable">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Subscription Interval</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($data)):  ?>
                                    <?php $i = 1;
                                    foreach($data as $data):  ?>
                                    <tr>
                                        <td><?php print $i; $i++; ?></td>
                                        <td><?php print $data->sub_interval;  ?></td>
                                        <td>
                                            <div class="actions">
                                                <!-- <a href="edit=<?php print $data->id;  ?>" class="btn btn-sm btn-success mr-2">
                                                    <i class="fas fa-pen"></i>
                                                </a> -->
                                                <a href="<?php print api_url('subscriptionDelete'); ?>?delete=<?php print $data->id; ?>" class="btn btn-sm btn-danger">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>                          
        </div>                  
    </div>
<script>
    function add_new_item() {
        let sub_interval = $('#interval').val();
        let sub_freequency = $('#freequency').val();

        let interval=sub_interval+' '+sub_freequency;

        if (sub_interval && sub_interval>0) 
        {
            $.post("<?=api_url('save_subscription')?>", {
                sub_interval: interval
            }).then((res, err) => {
                console.log(res, err);
            });
        }

        
    }
</script>