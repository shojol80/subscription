<?php

/*

type: layout

content_type: dynamic

name: Posts

position: 13

*/

?>
<style>

    .blog-sidebar h6 {
        color: #222222;
        text-transform: uppercase;
        font-size: 18px;
        letter-spacing: 0.1rem;
        font-weight: 700;
    }
</style>
<?php
if (!$classes['padding_top']) {
    $classes['padding_top'] = 'p-t-70';
}
if (!$classes['padding_bottom']) {
    $classes['padding_bottom'] = 'p-b-70';
}

$layout_classes = ' ' . $classes['padding_top'] . ' ' . $classes['padding_bottom'] . ' ';
?>

<section class="section <?php print $layout_classes; ?> edit safe-mode nodrop" field="layout-skin-13-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="allow-drop" rel="inherit">
                    <div class="sidebar">

                        <div class="sidebar__widget categorySideBar">
                            <h6><?php _lang("Kategorien", "templates/bamboo"); ?></h6>
                            <hr>
                            <div class="edit" field="blog_cat_content" rel="content">
                                <module type="categories" content-id="<?php print PAGE_ID; ?>"/>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <module type="posts" hide_paging="true" limit="4" />
            </div>
        </div>
    </div>
</section>
