<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" <?php print lang_attributes(); ?>>

<head>

    <title>{content_meta_title}</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="{content_meta_title}"/>
    <meta name="keywords" content="{content_meta_keywords}"/>
    <meta name="description" content="{content_meta_description}"/>
    <meta property="og:type" content="{og_type}"/>
    <meta property="og:url" content="{content_url}"/>
    <meta property="og:image" content="{content_image}"/>
    <meta property="og:description" content="{og_description}"/>
    <meta property="og:site_name" content="{og_site_name}"/>

    <script>
        mw.require('icon_selector.js');
        mw.lib.require('bootstrap4');
        mw.lib.require('bootstrap_select');

        mw.iconLoader()
            .addIconSet('fontAwesome')
            .addIconSet('iconsMindLine')
            .addIconSet('iconsMindSolid')
            .addIconSet('mwIcons')
            .addIconSet('materialIcons');
    </script>

    <script>
        $(document).ready(function () {
            $('.selectpicker').selectpicker();
        });
    </script>
    <?php
        print (template_head(true));
		//Seo data for google anaylytical
        $is_installed_status = Config::get('microweber.is_installed');
        (@$is_installed_status) ? basicGoogleAnalytical() : '';
        //end code
    ?>

    <!-- Plugins Styles -->
    <link href="<?php print template_url(); ?>assets/css/navigation.css" rel="stylesheet"/>
    <link href="<?php print template_url(); ?>assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet"/>

    <link href="<?php print template_url(); ?>assets/js/libs/swiper/css/swiper.min.css" rel="stylesheet"/>


    <link rel="stylesheet" href="<?php print template_url(); ?>assets/css/material_icons/material_icons.css" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <link href="<?php print template_url(); ?>assets/css/custom.css" rel="stylesheet"/>
    <link href="<?php print template_url(); ?>assets/css/style.css" rel="stylesheet"/>
    <link href="<?php print template_url(); ?>assets/css/responsive.css" rel="stylesheet"/>

    <link href="<?php print template_url(); ?>assets/css/responsive.css" rel="stylesheet"/>
    <link href="<?php print template_url(); ?>assets/css/typography.css" rel="stylesheet"/>
    <?php print get_template_stylesheet(); ?>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php print template_url(); ?>assets/js/jquery-ui.js"></script>
    <?php include('template_settings.php'); ?>
    <style>
        .exit-intent-popup {
            position: fixed;
            top: 50%;
            z-index: -1;
            left: 50%;
            transform: translate(-50%, -50%);
            visibility: hidden;
            transition: transform 0.3s cubic-bezier(0.4, 0.0, 0.2, 1);
        }



        .exit-intent-popup img {
            position: relative;
            max-width: 60%;
        }

        .exit-intent-popup span {
            position: absolute;
            cursor: pointer;
            top: 2px;
            right: 14px;
            z-index: 1 !important;
        }

        .close:hover {
            color: red;
            text-decoration: none;
        }

        .header-cart-link {
            display: block;
        }
        .newsletter {
            position: relative;
            background: #fff;
            max-width: 910px;
            overflow: hidden;
            box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
            min-width: 345px;
        }
        .mw-static-element.mw-image-and-text {
            padding: 50px 20px !important;
            max-width: 910px;
        }
        .mw-ui-col, .mw-ui-row-nodrop .mw-ui-col {
            display: unset !important;
            vertical-align: middle !important;
        }
        @media only screen and (max-width: 520px) {
            .exit-intent-popup img {
                position: relative;
                max-width: 100%;
            }
            .mw-static-element.mw-image-and-text {
                padding: 40px 0px !important;
                max-width: 910px;
            }
        }

        /* .bg-overlay {
            position: absolute;
            content: '';
            background: #00000063;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 1;
            overflow: hidden !important;
        } */
        .exit-intent-popup.visible {
            z-index: 99;
            visibility: visible;
            background: #00000063;
            width: 100%;
            height: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .hide{
            display: none !important;
        }
    </style>

</head>
<body class="<?php print helper_body_classes(); ?> <?php print 'member-nav-inverse ' . $header_style . ' ' . $sticky_navigation; ?> ">
<!-- <div class="custom-modal">
    <div class="exit-intent-popup" role="alert">

        <div class="newsletter">
        <span class="close" id='close_popup'>&times;</span>
            <div class="text-center">
                <div class="edit" rel="content" field="test_content">
                    <module type="layouts" template="skin-11"/>
                </div>
            </div>
        </div>
    </div>
</div> -->
<input type="hidden" id="page_id_for_layout_copy" value="<?=PAGE_ID;?>">
<?php

?>
<div class="main">
    <div class="navigation-holder">
        <nav class="navigation">
            <div class="container">
                <?php
                use Illuminate\Support\Facades\DB;
                    $headerShowCss = "none";
                    $temps = DB::table('header_show_hides')->get();
                    if($temps->count() > 0){
                        $pageCheck = false;
                        foreach( $temps as $temp){
                            $tempv = DB::table('content')->where('id',$temp->page_id)->get();
                            if(Request::url() == url('/').'/'.$tempv[0]->url){
                                $pageCheck = true;
                            }
                        }

                        if($pageCheck == true){
                            $headerShowCss = "none";
                        }
                        else{
                            $headerShowCss = "flex";
                        }
                    }
                    else{
                        $headerShowCss = "flex";
                    }

                    $sidebar = Config::get('custom.sidebar');
                    $header = Config::get('custom.header');
                    if(@$header == 'header'){
                        $showHeader = showCat('header');
                    }else if(@$sidebar == 'sidebar'){
                        $showHeader = showCat('sidebar');
                    }else{
                        Config::set('custom.sidebar', 'sidebar');
                        Config::save(array('custom'));
                    }

                ?>
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header" style="display:<?php print $headerShowCss; ?>;">
                    <module type="logo" class="logo" id="header-logo"/>

                    <div class="menu">
                        <module type="menu" id="header-menu" template="navbar"/>
                        <div class="header-cat <?php print  @$showHeader['header'] ?? 'hide'; ?>" >
                            <div class="header-categories">
                                <module type="categories" />
                            </div>
                        </div>
                        <ul class="list mobile-list">
                            <?php if ($profile_link == 'true'): ?>
                                <li class="mobile-profile">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="fa fa-user-circle-o"></i> <span><?php print user_name(); ?> <span class="caret"></span></span></a>
                                    <ul class="dropdown-menu">
                                        <?php if (user_id()): ?>
                                            <li><a href="#" data-toggle="modal" data-target="#loginModal">Profil</a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#ordersModal">Meine Bestellungen</a></li>
                                        <?php else: ?>
                                            <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _lang("Anmeldung", 'templates/bamboo'); ?></a></li>
                                        <?php endif; ?>

                                        <?php if (is_admin()): ?>
                                            <li><a href="<?php print admin_url() ?>">Adminbereich</a></li>
                                        <?php endif; ?>

                                        <?php if (user_id()): ?>
                                            <li><a href="<?php print api_link('logout') ?>"><?php _lang("Ausloggen", 'templates/bamboo'); ?></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>

                            <li class="mobile-search">
                                <form action="<?php print site_url(); ?>search.php" method="get">
                                    <i class="fa fa-search"></i>
                                    <input type="search" id="keywords" name="keywords" placeholder="<?php _lang("Search for products", 'templates/bamboo'); ?>"/>
                                    <button type="submit"><?php _lang("Search", 'templates/bamboo'); ?></button>
                                </form>
                            </li>
                        </ul>
                    </div>


                <div class="bamboo-hright" style="display:flex;align-items:center">


                    <div class="toggle">
                        <a href="javascript:;" class="js-menu-toggle">
                            <span class="mobile-menu-label">
                            </span>
                            <span class="mobile-menu-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </a>
                    </div>

                    <ul class="member-nav">
                        <li class="search dropdown">
                                <button class="btn-search dropdown-toggle" type="button" id="dropdown_search" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-search"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdown_search">
                                    <form class="d-flex flex-nowrap search-header-form" action="<?php print site_url(); ?>search" method="get">
                                        <input type="search" placeholder="" id="keywords" name="keywords"/>
                                        <button class="btn" type="submit"> Suchen</button>
                                    </form>
                                </div>
                            </li>

                        <?php if ($shopping_cart == 'true'): ?>
                            <li class="dropdown btn-cart">
                                <a href="#" class="dropdown-toggle" onclick="carttoggole()"><span class="material-icons cart-icon">
shopping_cart
</span> <span id="shopping-cart-quantity" class="js-shopping-cart-quantity"><?php print cart_sum(false); ?></span></a>

                            </li>
                        <?php endif; ?>


                            <li class="dropdown btn-member">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span><?php if (user_id()): ?><?php print user_name(); ?><?php else: ?><?php echo _e('Einloggen'); ?><?php endif; ?> <span class="caret"></span></span></a>
                                <ul class="dropdown-menu">
                                    <?php if (user_id()): ?>
                                        <li><a href="#" data-toggle="modal" data-target="#loginModal">Profil</a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#ordersModal">Meine Bestellungen</a></li>
                                    <?php else: ?>
                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _lang("Anmeldung", 'templates/bamboo'); ?></a></li>
                                    <?php endif; ?>

                                    <?php if (is_admin()): ?>
                                        <li><a href="<?php print admin_url() ?>">Adminbereich</a></li>
                                    <?php endif; ?>

                                    <?php if (user_id()): ?>
                                        <li><a href="<?php print api_link('logout') ?>"><?php _lang("Ausloggen", 'templates/bamboo'); ?></a></li>
                                    <?php endif; ?>
                                </ul>
                            </li>

                    </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <script>
	  $(document).ready(function(){
		  $('.js-example-basic-multiple').select2();
	  });
  </script>
<script type="text/javascript">
  function carttoggole() {
        $("#cartModal").modal('show');
    }
</script>

<!-- Login Modal -->
<div class="modal login-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">
                <div class="js-login-window">
                    <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <module type="users/login" id="loginModalModuleLogin" />
                    <!-- <div type="users/login" id="loginModalModuleLogin"></div> -->
                </div>

                <div class="js-register-window" style="display:none;">
                <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <module type="users/register" id="loginModalModuleRegister" />

                    <p class="or"><span>oder</span></p>

                    <div class="act login">
                        <a href="#" class="js-show-login-window"><span><i class="fa fa-backward" style="margin-right: 10px;" aria-hidden="true"></i>Zurück zur Anmeldung</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Cart Modal -->
<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <div id="checkout-product">
              <module type="shop/cart" template="quick_checkout" />
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>




<script>
    $(document).ready(function () {
        $('#loginModal').on('show.bs.modal', function (e) {
            $('#loginModalModuleLogin').reload_module();
            $('#loginModalModuleRegister').reload_module();
        })


        <?php if (isset($_GET['mw_payment_success'])) { ?>
        $('#js-ajax-cart-checkout-process').attr('mw_payment_success', true);

        <?php } ?>


        $('.js-show-register-window').on('click', function () {
            $('.js-login-window').hide();
            $('.js-register-window').show();
        })
        $('.js-show-login-window').on('click', function () {

            $('.js-register-window').hide();
            $('.js-login-window').show();
        })

        $(".header-cat .nav>li>ul>li").has("ul").addClass("sub-cat");
    })
</script>
<?php if (user_id()): ?>
    <script>
        $(document).ready(function () {
            $('#ordersModal').on('shown.bs.modal', function (e) {
                mw.reload_module('#user_orders_modal')
            });
        });
    </script>
    <!-- Orders Modal -->
    <div class="modal fade my-orders-modal" id="ordersModal" tabindex="-1" role="dialog" aria-labelledby="ordersModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>

                <div class="modal-body">
                    <div type="users/orders" id="user_orders_modal"></div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>


<?php
    dt_url_redirect_redirectUrl();
?>
