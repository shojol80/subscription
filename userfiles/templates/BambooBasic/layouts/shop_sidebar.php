<?php
$header = Config::get('custom.header');
$sidebar = Config::get('custom.sidebar');
if(@$header == 'header'){
    $showHeader = showCat('header');
}else if(@$sidebar == 'sidebar'){
    $showHeader = showCat('sidebar');
}

?>

<div class="allow-drop" rel="inherit">
    <div class="sidebar">
        <div class="sidebar__widget categorySideBar <?php print @$showHeader['sidebar']; ?>">
            <h6><?php _lang("Kategorien", "templates/bamboo"); ?></h6>
            <hr>
            <div class="edit" field="cat_content_main_wrapper" rel="content">
                <module type="categories" content-id="<?php print PAGE_ID; ?>"/>
            </div>
        </div>
        <div class="sidebar__widget wishlist_widget">
            <h6 class=""><?php _lang("Wunschzettel"); ?></h6>
            <hr>
            <div class="sidebar-box sidebar-custom-style">
                <ul class="mw-cats-menu" id="wishlist-list">

                </ul>
            </div>

            <?php if (is_logged()) { ?>
                <div id="wishlist-sidebar"></div>
                <p>&nbsp;</p>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">

Erstellen Sie eine neue Wunschliste
                </button>
            <?php } else { ?>
                <button data-toggle="modal" class="btn btn-primary login-modal" data-target="#loginModal">Login für Wunschliste</button>
            <?php } ?>
        </div>
        <div class="sidebar__widget edit" field="related_products_ab" rel="inherit">
            <h6>Über uns</h6>
            <hr>
            <div class="sidebar-custom-style edit" field="related_products_ab_text" rel="inherit">
                <p style="font-size:16px">
                    We're a digital focussed collective working with individuals and businesses to establish rich, engaging online presences.
                </p>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Titel der Wunschliste</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                               placeholder="Enter wishlist title">
                        <small id="emailHelp" class="form-text text-muted red" style="display: none;">Bitte geben Sie den Namen der Wunschliste ein</small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="btn btn-primary" onclick="create_sessions()">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenteredit" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Titel der Wunschliste</label>
                        <input type="text" class="form-control" id="exampleInputEmailedit" aria-describedby="emailHelp"
                               placeholder="Enter wishlist title">
                        <input type="hidden" class="form-control" id="exampleInputEmailedithide" aria-describedby="emailHelp"
                               placeholder="Enter wishlist title">
                        <small id="emailHelp" class="form-text text-muted red" style="display: none;">Bitte geben Sie den Namen der Wunschliste ein</small>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="btn btn-primary" onclick="edit_sessions()">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function create_sessions() {
        let title = $('#exampleInputEmail1').val();
        const emailHelp = $('#emailHelp');
        emailHelp.hide();
        if (title.trim().length > 0) {
            $.post("<?php print api_url('set_wishlist_sessions'); ?>", {title: title}, function (sessions) {
                if (sessions === 'false') {
                    emailHelp.show();
                } else {
                    location.reload();
                }
            });
        } else {
            emailHelp.show();
        }
    }

    function edit_sessions() {
        let title = $('#exampleInputEmailedit').val();
        let titlehide = $('#exampleInputEmailedithide').val();
        const emailHelp = $('#emailHelp');
        emailHelp.hide();
        if (title.trim().length > 0) {
            $.post("<?php print api_url('edit_wishlist_sessions'); ?>", {title: title, titlehide: titlehide}, function (sessions) {
                if (sessions === 'false') {
                    emailHelp.show();
                } else {
                    location.reload();
                }
            });
        } else {
            emailHelp.show();
        }
    }




    jQuery(window).on('load', function(){
        if(jQuery(".categorySideBar .module-categories>.well>ul.nav>li").children("ul").length) {
            jQuery(".categorySideBar .module-categories>.well>ul.nav>li").children("ul").parent().addClass("hasSubMenu");
            jQuery(".categorySideBar .module-categories>.well>ul.nav>li").children("ul").parent().append("<span class='hs-toggle'></span>");
        }

        jQuery(".hs-toggle").on("click", function(){
            // jQuery(".module-categories").toggleClass("showSub");
            $(".hs-toggle").parent().removeClass("showThisSub");
            $(this).parent().addClass("showThisSub");

        });


        if(jQuery(".categorySideBar .module-categories>.well>ul.nav>li").length>5){
            jQuery(".categorySideBar").append("<span class='viewMoreCategory'>weitere anzeigen</span>");
        }

        jQuery(".viewMoreCategory").on("click", function(){
            jQuery(".categorySideBar .module-categories>.well>ul.nav").toggleClass("show_ucmAll");

            var currentVMbtnText = jQuery(".viewMoreCategory").text();
            if (currentVMbtnText === "weitere anzeigen") {
                jQuery(".viewMoreCategory").html("ausblenden");
            } else {
                jQuery(".viewMoreCategory").html("weitere anzeigen");
            }
            //alert(currentVMbtnText);
        });

    });

</script>
