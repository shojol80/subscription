<?php

namespace MicroweberPackages\Install\Schema;

class Droptienda
{
    public function get()
    {
        return [
            'wishlist_sessions' => [
                'user_id' => 'integer',
                'name' => 'string',
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
            ],

            'wishlist_session_products' => [
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
                'user_id' => 'integer',
                'wishlist_id' => 'integer',
                'product_id' => 'integer',

            ],

            'dynamic_banners' => [
                'user_id' => 'integer',
                'media_id' => 'integer',
                'coords' => 'string',
                'product_id' => 'integer',
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
            ],

            'wishlist_link' => [
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
                'user_id' => 'integer',
                'products_id' => 'string',
                'slug' => 'string',
            ],

            'quick_checkout' => [
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
                'user_id' => 'integer',
                'products_id' => 'integer',
                'slug' => 'string',
                'is_deliver' => 'string',

            ],

            'legals' => [
                'updated_at' => 'dateTime',
                'created_at' => 'dateTime',
                'term_name' => 'string',
                'description' => 'longText',
                'counter' => 'string',

            ],

            'sync_history' => [
                'sync_type' => 'string',
                'sync_event' => 'string',
                'model_id' => 'integer',
                'synced_at' => 'dateTime',
                'drm_ref_id' => 'integer',
                'tries' => array('type' => 'tinyInteger', 'default' => 0),
                'exception' => 'text',
                'response' => 'text',
                'created_at' => 'dateTime',
                'updated_at' => 'dateTime',
                'deleted_at' => 'dateTime',
            ],

            'tokenurl' => [
                'token' => 'string',
                'url' => 'string',
            ],

            'header_show_hides' => [
                'page_id' => 'integer',
            ],

            'product_upselling' => [
                'serviceName' => 'string',
                'servicePrice' => 'string',
            ],

            'product_upselling_item' => [
                'product_id' => 'integer',
                'item_id' => 'integer',
                'selected' => 'integer',
            ],

            'selected_product_upselling_item' => [
                'product_id' => 'integer',
                'service_id' => 'integer',
                'service_price' => 'string',
                'user_id' => 'integer',
            ],

            'order_with_upselling' => [
                'product_id' => 'integer',
                'service_id' => 'integer',
                'user_id' => 'integer',
                'order_id' => 'integer',
            ],

            'checkout_bumbs' => [
                'product_id' => 'integer',
                'show_cart' => 'integer',
                'show_checkout' => 'integer',
            ],

            'thank_you_pages' => [
                'template_name' => 'integer',
                'product_id' => 'integer',
                'is_active' => 'integer',
            ],

            'iconImage' => [
                'name' => 'string',
                'iid' => 'integer',
            ],

            'delete_product_info' => [
                'product_url' => 'string',
                'category_url' => 'string',
            ],

            'variants' => [
                'rel_id' => 'string',
                'title' => 'string',
                'price' => 'string',
                'uvp' => 'string',
                'ean' => 'string',
                'sku' => 'string',
                'color' => 'string',
                'size' => 'string',
                'materials' => 'string',
                'drm_ref_id' => 'string',
                'description' => 'string',
                'stock' => 'string',
            ],

            'product_variants' => [
                'user_id' => 'string',
                'variant_id' => 'string',
                'content_id' => 'string',
                'varianted_price' => 'string',
            ],

            'tax_rates' => [
                'country' => 'string',
                'country_de' => 'string',
                'country_code' => 'string',
                'charge' => 'string',
                'created_at' => 'string',
                'updated_at' => 'string',
                'lang_kod' => 'string',
                'alpha_three' => 'string',
                'is_default' => 'string',
            ],

            'not_deletable' => [
                'rel_id' => 'string',
            ]


        ];
    }
}
