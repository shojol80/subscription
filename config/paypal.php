<?php

return array(

    /**
     * Set our Sandbox and Live credentials
     */
    'sandbox_client_id' => 'Ae9PoIDnWybwVHlxAAZj5DIuQoG4CF1wIQm3kjKrEAnT1CbYNMrlWlGjqpFXt5fWP3GAWiJLqj9JV6xL',
    'sandbox_secret' => 'EK5GDlzmwQAlKkGdBfxeqm6SCUnatkuHyzriGSMW8_jCnH_foCykYJ_J_ImMlaYQ_GLQ5qaxiTVg2w-L',
    'live_client_id' => 'Ae9PoIDnWybwVHlxAAZj5DIuQoG4CF1wIQm3kjKrEAnT1CbYNMrlWlGjqpFXt5fWP3GAWiJLqj9JV6xL',
    'live_secret' => 'EK5GDlzmwQAlKkGdBfxeqm6SCUnatkuHyzriGSMW8_jCnH_foCykYJ_J_ImMlaYQ_GLQ5qaxiTVg2w-L',


    /**
     * SDK configuration settings
     */
    'settings' => array(

        /**
         * Payment Mode
         *
         * Available options are 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        // Specify the max connection attempt (3000 = 3 seconds)
        'http.ConnectionTimeOut' => 3000,

        // Specify whether or not we want to store logs
        'log.LogEnabled' => true,

        // Specigy the location for our paypal logs
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Log Level
         *
         * Available options: 'DEBUG', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the DEBUG level and decreases
         * as you proceed towards ERROR. WARN or ERROR would be a
         * recommended option for live environments.
         *
         */
        'log.LogLevel' => 'DEBUG'
    ),
);
