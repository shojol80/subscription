<?php return array (
  'providers' => 
  array (
    0 => 'Arcanedev\\SeoHelper\\SeoHelperServiceProvider',
    1 => 'Barryvdh\\Debugbar\\ServiceProvider',
    2 => 'Barryvdh\\DomPDF\\ServiceProvider',
    3 => 'L5Swagger\\L5SwaggerServiceProvider',
    4 => 'Facade\\Ignition\\IgnitionServiceProvider',
    5 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    6 => 'Fruitcake\\Cors\\CorsServiceProvider',
    7 => 'GrahamCampbell\\Markdown\\MarkdownServiceProvider',
    8 => 'Jenssegers\\Agent\\AgentServiceProvider',
    9 => 'Laravel\\Socialite\\SocialiteServiceProvider',
    10 => 'Laravel\\Tinker\\TinkerServiceProvider',
    11 => 'Collective\\Html\\HtmlServiceProvider',
    12 => 'Mtrajano\\LaravelSwagger\\SwaggerServiceProvider',
    13 => 'Carbon\\Laravel\\ServiceProvider',
    14 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    15 => 'Conner\\Tagging\\Providers\\TaggingServiceProvider',
    16 => 'Spatie\\Permission\\PermissionServiceProvider',
    17 => 'EloquentFilter\\ServiceProvider',
    18 => 'MicroweberPackages\\App\\Providers\\AppServiceProvider',
    19 => 'MicroweberPackages\\App\\Providers\\EventServiceProvider',
    20 => 'MicroweberPackages\\App\\Providers\\RouteServiceProvider',
  ),
  'eager' => 
  array (
    0 => 'Barryvdh\\Debugbar\\ServiceProvider',
    1 => 'Barryvdh\\DomPDF\\ServiceProvider',
    2 => 'L5Swagger\\L5SwaggerServiceProvider',
    3 => 'Facade\\Ignition\\IgnitionServiceProvider',
    4 => 'Fideloper\\Proxy\\TrustedProxyServiceProvider',
    5 => 'Fruitcake\\Cors\\CorsServiceProvider',
    6 => 'GrahamCampbell\\Markdown\\MarkdownServiceProvider',
    7 => 'Jenssegers\\Agent\\AgentServiceProvider',
    8 => 'Mtrajano\\LaravelSwagger\\SwaggerServiceProvider',
    9 => 'Carbon\\Laravel\\ServiceProvider',
    10 => 'NunoMaduro\\Collision\\Adapters\\Laravel\\CollisionServiceProvider',
    11 => 'Conner\\Tagging\\Providers\\TaggingServiceProvider',
    12 => 'Spatie\\Permission\\PermissionServiceProvider',
    13 => 'EloquentFilter\\ServiceProvider',
    14 => 'MicroweberPackages\\App\\Providers\\AppServiceProvider',
    15 => 'MicroweberPackages\\App\\Providers\\EventServiceProvider',
    16 => 'MicroweberPackages\\App\\Providers\\RouteServiceProvider',
  ),
  'deferred' => 
  array (
    'Arcanedev\\SeoHelper\\Contracts\\SeoHelper' => 'Arcanedev\\SeoHelper\\SeoHelperServiceProvider',
    'Arcanedev\\SeoHelper\\Contracts\\SeoMeta' => 'Arcanedev\\SeoHelper\\SeoHelperServiceProvider',
    'Arcanedev\\SeoHelper\\Contracts\\SeoOpenGraph' => 'Arcanedev\\SeoHelper\\SeoHelperServiceProvider',
    'Arcanedev\\SeoHelper\\Contracts\\SeoTwitter' => 'Arcanedev\\SeoHelper\\SeoHelperServiceProvider',
    'Laravel\\Socialite\\Contracts\\Factory' => 'Laravel\\Socialite\\SocialiteServiceProvider',
    'command.tinker' => 'Laravel\\Tinker\\TinkerServiceProvider',
    'html' => 'Collective\\Html\\HtmlServiceProvider',
    'form' => 'Collective\\Html\\HtmlServiceProvider',
    'Collective\\Html\\HtmlBuilder' => 'Collective\\Html\\HtmlServiceProvider',
    'Collective\\Html\\FormBuilder' => 'Collective\\Html\\HtmlServiceProvider',
  ),
  'when' => 
  array (
    'Arcanedev\\SeoHelper\\SeoHelperServiceProvider' => 
    array (
    ),
    'Laravel\\Socialite\\SocialiteServiceProvider' => 
    array (
    ),
    'Laravel\\Tinker\\TinkerServiceProvider' => 
    array (
    ),
    'Collective\\Html\\HtmlServiceProvider' => 
    array (
    ),
  ),
);